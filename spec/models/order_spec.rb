require 'rails_helper'

RSpec.describe Order, :type => :model do
  fixtures :orders
  it "has a sneakers" do
    expect(orders(:sneakers).code).to eq(1)
    expect(orders(:sneakers).name).to eq("sneakers")
  end
end
